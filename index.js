console.log('Hello, World')
// Details
const details = {
    fName: "Marcus",
    lName: "Catajay",
    age: 17,
    hobbies : [
        "basketball", "watching anime", "playing games"
    ] ,
    workAddress: {
        housenumber: "Lot 36, Block 7",
        street: "Venus Street",
        city: "Caloocan City",
        state: "United States",
    }
}
const work = Object.values(details.workAddress);
console.log("My First Name is " + details.fName)
console.log("My Last Name is " + details.lName)
console.log(`Yes, I am ${details.fName} ${details.lName}.`)
console.log("I am " + details.age + " years old.")
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I work at " + work.join(", ") + ".");